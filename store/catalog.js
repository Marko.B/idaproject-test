export const state = () => ({
  cards: [],
});

export const getters = {
  getCards: (state) => {
    return state.cards;
  },
};

export const mutations = {
  addCard(state, card) {
    state.cards.push(card);
    
    localStorage.setItem("cards", JSON.stringify(state.cards));
  },
  removeCard(state, i) {
    state.cards.splice(i, 1);
    
    localStorage.setItem("cards", JSON.stringify(state.cards));
  },
  setCards(state) {
    let cards = localStorage.getItem("cards");

    state.cards = cards ? JSON.parse(cards) : [];
  },
};
